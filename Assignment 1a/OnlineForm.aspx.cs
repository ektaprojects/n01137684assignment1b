﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment_1a
{
    public partial class OnlineForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
               
         
        }

        /* Reference: https://stackoverflow.com/questions/1469280/asp-net-datetime-picker */
        protected void DateChange(object sender, EventArgs e)
        {
            deliveryDate.Text = deliveryCal.SelectedDate.ToShortDateString() + '.';
        }

        protected void Order(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            /* Get the information for Customer and create customer object */

            string fname = customerFName.Text.ToString();
            string lname = customerLName.Text.ToString();
            string phone = customerPhone.Text.ToString();
            string email = customerEmail.Text.ToString();
            Customer newcustomer = new Customer(fname,lname,phone,email);

            /* Creating item object and get information for item */

            string selectedItem = item.SelectedItem.Value.ToString();
            string sz = size.SelectedItem.Value.ToString();
            List<string> colors = new List<string> { };
            int qty = int.Parse(itemQty.Text);
            Item newitem = new Item(selectedItem,sz,colors,qty);
           
            //Reference: Cristine's code pizza toppings
            //add items to list
            foreach(Control control in itemcolors.Controls)
            {
                if(control.GetType() == typeof(CheckBox))
                {
                    CheckBox itemclr = (CheckBox)control;
                    if(itemclr.Checked)
                    {
                        colors.Add(" "+itemclr.Text);
                    }
                }
            }


            /* Creating delivery object and get information for delivery */

            string add = deliveryAddress.Text.ToString();
            string c = deliveryCity.Text.ToString();
            string pro = deliveyProvince.Text.ToString();
            string con = deliveryCountry.Text.ToString();
            string pos = deliveryPostal.Text.ToString();
            /* https://stackoverflow.com/questions/45082491/how-to-get-datepicker-value-in-code-behind-in-asp-net-webforms-on-button-click */
            DateTime ddate = Convert.ToDateTime(deliveryDate.Text);
            Delivery newdelivery = new Delivery();
            newdelivery.DAddress = add;
            newdelivery.DCity = c;
            newdelivery.DProvince = pro;
            newdelivery.DCountry = con;
            newdelivery.DPostal = pos;
            newdelivery.DDate = ddate;


            /* Object of Order classs */

            Order neworder = new Order(newcustomer,newitem,newdelivery);

            OrderDet.InnerHtml = neworder.printInvoice();
        }
    }

}